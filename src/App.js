import React from 'react';
import NoteList from './components/NoteList.js';
import Form from './components/Form.js';
import './style/App.css';
import { StoreProvider } from './store/useStore';
import { ThemeProvider } from 'emotion-theming';
import theme from '@rebass/preset';
import { Box, Heading, Text, Flex } from 'rebass';

const App = () => {
    return (
        <ThemeProvider theme={theme}>
            <StoreProvider>
                <Flex flexWrap='wrap'>
                    <Box p={3}>
                        <Heading variant='display' color='#07E8D5'>Welcome To Notiz</Heading>
                        <Text mb={2}>A simple app to keep small notes</Text>
                    </Box>
                </Flex>
                <div className="wrapper-list">
                    <NoteList/>
                </div>
                <Form/>
            </StoreProvider>
        </ThemeProvider>
    );
};

export default App;
