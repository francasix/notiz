import React from 'react';
import { Input, Checkbox, Label } from '@rebass/forms';
import { Box } from 'rebass';

export const Field = (data) => {
    const { field, setField, checked, value } = data;
    return (
        <Box>
            {field.type === 'checkbox' ? <Label p={2}>
                <Checkbox
                    checked={checked}
                    onChange={e => setField[field.type](e.target.checked)}
                />
                {field.label}
            </Label> : <Input id={field.label}
                              p={[3, 2, 2]}
                              onChange={e => setField[field.type](e.target.value)}
                              value={value}
                              type={field.type}
                              placeholder={field.placeholder}/>
            }

        </Box>
    );
};