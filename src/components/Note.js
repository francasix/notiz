import React from 'react';
import { Box, Text } from 'rebass';
import { ReactComponent as PrivateLogo } from './../assets/anonymous.svg';

export const Note = ({ message, privacy, date, colour, index, nbElements }) => {
    return <Box
        className='relative'
        data-testid={index}
        p={4}
        width={[1, 1, 1 / (nbElements > 3 ? 3 : nbElements)]}
        color='white'
        bg={colour}>
        {privacy ? <PrivateLogo className='svg-privacy'/> : ''}
        <Text fontSize={4}>{message}</Text>
        <Text fontSize={0}>{date}</Text>
    </Box>;
};