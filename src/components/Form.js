import React, { useEffect, useState } from 'react';
import FormFromAPI from '../data/static-form.json';
import { useStore } from '../store/useStore';
import { Field } from './Input.js';
import { Box, Flex, Button } from 'rebass';

const Form = () => {
    const { state: { form }, dispatch } = useStore();
    const [message, setMessage] = useState('');
    const [privacy, setPrivacy] = useState(true);

    async function addNote(e){
        e.preventDefault();
        if (message) {
            await dispatch({ type: 'ADD_NOTE', payload: { message, privacy } });
            setMessage('');
        }
    }

    useEffect(() => {
        const loadForm = () => {
            dispatch({ type: 'UPDATE_FORM', payload: FormFromAPI.response });
        };
        !form.fields && !form.actions && loadForm();
    }, [form, dispatch]);

    return (
        <Flex className="wrapper-form">
            <Box width={[1, 1, 1 / 2]} p={3}>
                <form>
                    <Flex>
                        <Box width={2 / 3}>
                            {form.fields && form.fields.map((field, i) => {
                                return <Field
                                    key={i}
                                    setField={{ 'checkbox': setPrivacy, 'text': setMessage }}
                                    checked={privacy}
                                    value={message}
                                    field={field}
                                />;
                            })}
                        </Box>
                        <Box width={1 / 3}>
                            {form.actions && form.actions.map((field, i) => {
                                return <Button p={[3, 2, 2]} type="submit" key={i}
                                               onClick={addNote}>{field.label}</Button>;
                            })}
                        </Box>
                    </Flex>
                </form>
            </Box>
        </Flex>
    );
};

export default Form;
