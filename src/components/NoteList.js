import React, { useEffect, useState } from 'react';
import { useStore } from '../store/useStore';
import NotesFromAPI from '../data/static-empty-notes.json';
import { Flex } from 'rebass';
import { Note } from './Note';

const NoteList = () => {
    const { state, dispatch } = useStore();
    const [dataResponse, setDataResponse] = useState(false);

    useEffect(() => {
        const loadNotes = () => {
            dispatch({ type: 'UPDATE_NOTES', payload: NotesFromAPI.response });
            setDataResponse(true);
        };
        !dataResponse && loadNotes();
    }, [state, dispatch, dataResponse]);

    return (
        <Flex flexWrap='wrap'>
            {state.notes && state.notes.map((note, i) => {
                const { message, colour, privacy, date } = note;
                return <Note index={i}
                             colour={colour}
                             date={date}
                             privacy={privacy}
                             message={message}
                             key={`${date}-${colour}`}
                             nbElements={state.notes.length}/>;
            })}
        </Flex>
    );
};

export default NoteList;
