import { getFrenchDate, getFrenchDateNow, getRandomFilteredItem, getObjectsValue } from '../helpers';
import data from '../config.json';

const initialState = {
    notes: [],
    form: {}
};

function notesByIndex(array, index, key){
    let arr = [];
    for (let j = 0; j <= array.length; j++) {
        arr = index.map((i) => array[i]).filter(x => x);
    }
    return getObjectsValue(arr, key);
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_NOTE':
            const { message, privacy, date, colour } = action.payload;
            const randomColour = getRandomFilteredItem(data.colours, notesByIndex(state.notes, [0, 2], 'colour'));
            return {
                ...state,
                notes: [Object.assign({}, {
                    message,
                    date: date || getFrenchDateNow(),
                    privacy,
                    colour: colour || randomColour
                }), ...state.notes]
            };
        case 'UPDATE_NOTES':
            const { notes } = action.payload;
            return {
                ...state,
                notes: notes.map((note, i) => {
                    return { ...note, date: getFrenchDate(note.date) };
                })
            };
        case 'UPDATE_FORM':
            const { fields, actions } = action.payload;
            return {
                ...state,
                form: { fields, actions }
            };
        default:
            return state;
    }
};

export default reducer;