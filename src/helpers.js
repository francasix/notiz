export const getFrenchDate = (date) => new Date(parseFloat(date)).toLocaleString('fr-FR');

export const getFrenchDateNow = () => new Date().toLocaleString('fr-FR');

const filteredArray = (itemsToRemove, array) => itemsToRemove ? array.filter((item) => !itemsToRemove.includes(item)) : array;

export const getRandomFilteredItem = (array, avoid) => {
    const newArray = filteredArray(avoid, array);
    return newArray[Math.floor(Math.random() * newArray.length)];
};

export const getObjectsValue = (array, key) => array.map((item) => item ? item[key] : null).filter(x => x);
