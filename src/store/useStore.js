import React, { useReducer, createContext, useContext } from 'react';
import reducer from '../reducers/reducer';

const initialState = { notes: [], form: {} };
const StoreContext = createContext(initialState);
export const { Provider } = StoreContext;

export const StoreProvider = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    return (
        <Provider value={{ state, dispatch }}>
            {children}
        </Provider>
    );
};

export const useStore = () => {
    const { state, dispatch } = useContext(StoreContext);
    return { state, dispatch };
};