import React from 'react';
import { render, cleanup } from '@testing-library/react';
import App from '../App';
import NoteList from '../components/NoteList';
import { StoreProvider, Provider } from '../store/useStore';

const initialState = { notes: [{ 'message': 'My first note' }], form: {} };
const renderWithContext = (
    component) => {
    return {
        ...render(
            <StoreProvider>
                <Provider value={{
                    state: initialState, dispatch: () => {
                    }
                }}>{component}</Provider>
            </StoreProvider>)
    };
};

afterEach(cleanup);

test('App snapshot', () => {
    const { asFragment } = render(<App/>);
    expect(asFragment(<App/>)).toMatchSnapshot();
});

test('NoteList', () => {
    const { getByTestId } = renderWithContext(<NoteList/>);
    expect(getByTestId('0')).toHaveTextContent('My first note');
});
