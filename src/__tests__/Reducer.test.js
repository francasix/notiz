import reducer from '../reducers/Reducer';
import { getFrenchDate } from '../helpers';

const initialState = {
    notes: [],
    form: {}
};
const message = {'message': 'Un message'};
const noteFromAPI = {message, 'date': 1598091152944, 'privacy': false, 'colour': '#F205CB'};
const notes = {notes: [noteFromAPI], form: {}};
const note = {message, 'date': getFrenchDate(1598091152944), 'privacy': false, 'colour': '#F205CB'};
const emptyForm = {fields: [], actions: []};

test('reducer returns initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
});

describe('Reducer Form', () => {
    it('should handle UPDATE_FORM', () => {
        const action = {
            type: 'UPDATE_FORM',
            payload: emptyForm
        };
        expect(reducer(initialState, action)).toEqual({form: emptyForm, notes: []});
    });
});

describe('Reducer NoteList', () => {
    it('should handle ADD_NOTE', () => {
        const action = {
            type: 'ADD_NOTE',
            payload: note
        };

        expect(reducer(initialState, action)).toEqual({notes: [note], form: {}});
    });

    it('should handle UPDATE_NOTES', () => {
        const action = {
            type: 'UPDATE_NOTES',
            payload: notes
        };
        expect(reducer(initialState, action)).toEqual({notes: [note], form: {}});
    });
});