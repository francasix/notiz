![desktop version](https://bitbucket.org/francasix/notiz/raw/8de2ce3c92f304ad7a6d5ad1a79d844bd304cc08/public/Screenshot%202020-08-23%20at%2019.48.55.png)

# Notiz

A simple app to keep small notes!

## Introduction

Notiz is a frontend side React application, based on the great starter project [Create React App](https://github.com/facebook/create-react-app).

### How it works?

#### Starting point

As all React Application entry point `#root` is in `index.html` and first render is in `index.js`.

Main parent component is in `App.js`.

#### State & Reducer

In /store Notiz project uses a `useStore()` hook to manage a global state of the app.

`reducer.js` handles actions such as 'UPDATE' or 'ADD' of the global state. 

Components mainly use hook feature of React 16.

#### API Rest friendly

Fetch data from an API can be added in `useEffect()` of the components. Some fake API responses are available in /data.

#### Config

`config.json` contains local data used by several components or service.

#### CSS 

The css framework [rebass](https://rebassjs.org/) gives a lot of nice components using Flexbox system.

### Quick Start Development

Main development branch is `develop-1`.


```
$ git clone
$ cd notiz
$ yarn 
```

**Compile App.scss to App.css with your favourite IDE.**

```
$ yarn start
$ yarn test
```

![app launched](https://bitbucket.org/francasix/notiz/raw/8de2ce3c92f304ad7a6d5ad1a79d844bd304cc08/public/Screenshot%202020-08-23%20at%2019.07.53.png)


## Going further

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
